# Simple RESTful API
  the specification :
  
  * Spring-Boot (2.5.3)
  * Gradle
  * Kotlin (1.5.21)
  * JVM 1.8
  * PostgreSql Database

## Script

   you can change some configuration on [application.yml](src/main/resources/application.yml)
   
   * Running Spring-Boot with Gradle
   
     ```
     gradle bootRun
     ``` 
     
     application will run on [http://localhost:8080/resource-server](http://localhost:8080/resource-server)

## System Design

   ![system design picture](documentation/images/system_design.png)
   
## API Specification

* [Product](documentation/api-specification/product.md)

  `localhost:8080/resource-server/api/product/..`
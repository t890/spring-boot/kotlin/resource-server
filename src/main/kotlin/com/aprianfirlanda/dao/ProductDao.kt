package com.aprianfirlanda.dao

import com.aprianfirlanda.error.NoContentException
import com.aprianfirlanda.model.FindRequest
import com.aprianfirlanda.model.ProductRequest
import com.aprianfirlanda.model.ProductResponse
import com.aprianfirlanda.utils.QueryPage
import com.aprianfirlanda.utils.QuerySort
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository

@Repository
class ProductDao(val jdbcTemplate: NamedParameterJdbcTemplate) {

    fun find(request: FindRequest<ProductRequest.Find>): Pair<List<ProductResponse>, Long> {
        val sql = "select\n" +
                "   id          as id,\n" +
                "   name        as name,\n" +
                "   price       as price,\n" +
                "   quantity    as quantity,\n" +
                "   created_at  as createdAt,\n" +
                "   updated_at  as updatedAt\n" +
                "from products\n" +
                "where 1  = 1\n"

        val (sqlFiltered, params) = findFilter(sql, request)

        /* Sort data */
        val sqlSorted = sqlFiltered + QuerySort(request.orderBy, request.orderDir).query

        /* Limit Offset data */
        var sqlLimited = sqlSorted
        if (request.size != -1)
            sqlLimited = sqlSorted + QueryPage(request.size, request.page).query

        val list = jdbcTemplate.query(sqlLimited, params) { rs, _ ->
            ProductResponse(
                id = rs.getString("id"),
                name = rs.getString("name"),
                price = rs.getLong("price"),
                quantity = rs.getInt("quantity"),
                createdAt = rs.getDate("createdAt"),
                updatedAt = rs.getDate("updatedAt")
            )
        }

        val sqlCount = "select count(*) as row from (\n" +
                sqlFiltered +
                ") tableCount"

        val totalData: Long = jdbcTemplate.queryForObject(sqlCount, params) { rs, _ ->
            rs.getLong("row")
        } ?: throw NoContentException()

        return Pair(list, totalData)
    }

    private fun findFilter(query: String, request: FindRequest<ProductRequest.Find>): Pair<String, MapSqlParameterSource> {
        val queryfilter = StringBuilder(query)
        val params = MapSqlParameterSource()

        /* filter data with where */
        val filter = request.filter
        if (filter != null){
            if (!filter.name.isNullOrBlank()) {
                queryfilter.append("   and lower(name) like lower(:name)\n")
                params.addValue("name", "%".plus(filter.name).plus("%"))
            }

            if (filter.price != null) {
                queryfilter.append("   and price = :price\n")
                params.addValue("price", filter.price)
            }

            if (filter.quantity != null) {
                queryfilter.append("   and quantity = :quantity\n")
                params.addValue("quantity", filter.quantity)
            }
        }

        return Pair(queryfilter.toString(), params)
    }
}
package com.aprianfirlanda.repository

import com.aprianfirlanda.entity.Product
import org.springframework.data.jpa.repository.JpaRepository

interface ProductRepository : JpaRepository<Product, String> {

}
package com.aprianfirlanda.model

data class FindResponse<T>(

    val page: Int,

    val size: Int,

    val totalData: Long,

    val totalPage: Int,

    val totalDataOnPage: Int,

    val data: T
)
package com.aprianfirlanda.model

import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

class ProductRequest {

    data class Create (

        @field:NotNull
        @field:NotBlank
        val name: String?,

        @field:NotNull()
        @field:Min(value = 1)
        val price: Long?,

        @field:NotNull
        @field:Min(value = 0)
        val quantity: Int?

    )

    data class Update (

        @field:NotNull
        @field:NotBlank
        val id: String?,

        @field:NotNull
        @field:NotBlank
        val name: String?,

        @field:NotNull
        @field:Min(value = 1)
        val price: Long?,

        @field:NotNull
        @field:Min(value = 0)
        val quantity: Int?

    )

    data class Find (

        val name: String?,

        val price: Long?,

        val quantity: Int?

    )

}
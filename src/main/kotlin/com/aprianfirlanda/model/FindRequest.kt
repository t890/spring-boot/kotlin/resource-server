package com.aprianfirlanda.model

data class FindRequest<T> (

    val page: Int,

    val size: Int,

    val orderBy: Int,

    val orderDir: String,

    val filter: T?
)
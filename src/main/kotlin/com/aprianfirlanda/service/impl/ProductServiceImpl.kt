package com.aprianfirlanda.service.impl

import com.aprianfirlanda.dao.ProductDao
import com.aprianfirlanda.repository.ProductRepository
import com.aprianfirlanda.entity.Product
import com.aprianfirlanda.error.NoContentException
import com.aprianfirlanda.model.FindRequest
import com.aprianfirlanda.model.FindResponse
import com.aprianfirlanda.model.ProductRequest
import com.aprianfirlanda.model.ProductResponse
import com.aprianfirlanda.service.ProductService
import com.aprianfirlanda.utils.DataPage
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*

@Service
class ProductServiceImpl(
    val repository: ProductRepository,
    val dao: ProductDao
): ProductService {

    override fun save(createValue: ProductRequest.Create): ProductResponse {
        val entity = Product(
            id = null,
            name = createValue.name!!,
            price = createValue.price!!,
            quantity = createValue.quantity!!,
            createdAt = Date(),
            updatedAt = null
        )

        val saved = repository.save(entity)

        return convertEntityToResponse(saved)
    }

    override fun findById(id: String): ProductResponse {
        val entity = repository.findByIdOrNull(id) ?: throw NoContentException()

        return convertEntityToResponse(entity)
    }

    override fun update(updateValue: ProductRequest.Update): ProductResponse {
        val entity = repository.findByIdOrNull(updateValue.id) ?: throw NoContentException()

        entity.apply {
            name = updateValue.name!!
            price = updateValue.price!!
            quantity = updateValue.quantity!!
            updatedAt = Date()
        }

        val updated = repository.save(entity)

        return convertEntityToResponse(updated)
    }

    override fun deleteById(id: String): String {
        val isExists = repository.existsById(id)

        if (!isExists) throw NoContentException()

        repository.deleteById(id)

        return id
    }

    override fun find(findRequest: FindRequest<ProductRequest.Find>): FindResponse<List<ProductResponse>> {

        val (productsResponse, totalData) = dao.find(findRequest)
        val totalPage = DataPage(totalData, findRequest.size).totalPage()

        return FindResponse(
            page = findRequest.page,
            size = findRequest.size,
            totalData = totalData,
            totalDataOnPage = productsResponse.size,
            totalPage = totalPage,
            data = productsResponse
        )
    }

    private fun convertEntityToResponse(product: Product): ProductResponse {
        return ProductResponse(
            id = product.id,
            name = product.name,
            price = product.price,
            quantity = product.quantity,
            createdAt = product.createdAt,
            updatedAt = product.updatedAt
        )
    }

}
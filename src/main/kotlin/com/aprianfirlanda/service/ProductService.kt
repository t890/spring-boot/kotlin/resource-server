package com.aprianfirlanda.service

import com.aprianfirlanda.model.FindRequest
import com.aprianfirlanda.model.FindResponse
import com.aprianfirlanda.model.ProductRequest
import com.aprianfirlanda.model.ProductResponse

interface ProductService {

    fun save(createValue: ProductRequest.Create): ProductResponse

    fun findById(id: String): ProductResponse

    fun update(updateValue: ProductRequest.Update): ProductResponse

    fun deleteById(id: String): String

    fun find(findRequest: FindRequest<ProductRequest.Find>): FindResponse<List<ProductResponse>>

}
package com.aprianfirlanda

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinResourceServerApplication

fun main(args: Array<String>) {
	runApplication<KotlinResourceServerApplication>(*args)
}

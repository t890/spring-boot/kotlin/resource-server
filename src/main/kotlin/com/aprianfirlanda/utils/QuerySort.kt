package com.aprianfirlanda.utils

class QuerySort(by: Int, dir: String) {
    val query = "order by " + by.plus(1) + " " + dir + "\n"
}
package com.aprianfirlanda.utils

class DataPage(private val totalData: Long, private val size: Int) {

    fun totalPage(): Int {
        return if ((totalData % size).toInt() == 0)
            (totalData / size).toInt()
        else
            (totalData / size).toInt() + 1
    }

}
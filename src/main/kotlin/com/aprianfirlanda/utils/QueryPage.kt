package com.aprianfirlanda.utils

class QueryPage(size: Int, page: Int) {
    val query = "limit " + size + " offset " + page.times(size) + "\n"
}
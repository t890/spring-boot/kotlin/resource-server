package com.aprianfirlanda.controller

import com.aprianfirlanda.model.FindRequest
import com.aprianfirlanda.model.FindResponse
import com.aprianfirlanda.model.ProductRequest
import com.aprianfirlanda.model.ProductResponse
import com.aprianfirlanda.service.ProductService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/api/product")
class ProductController(val service: ProductService) {

    @PostMapping(
        value = ["/save"],
        produces = ["application/json"],
        consumes = ["application/json"]
    )
    @ResponseStatus(HttpStatus.CREATED)
    fun save(@RequestBody @Valid createValue: ProductRequest.Create): ProductResponse {
        return service.save(createValue)
    }

    @GetMapping(
        value = ["/find-by-id/{id}"],
        produces = ["application/json"]
    )
    @ResponseStatus(HttpStatus.OK)
    fun findById(@PathVariable("id") id: String): ProductResponse {
        return service.findById(id)
    }

    @PutMapping(
        value = ["/update"],
        produces = ["application/json"],
        consumes = ["application/json"])
    @ResponseStatus(HttpStatus.OK)
    fun update(@RequestBody @Valid updateValue: ProductRequest.Update): ProductResponse {
        return service.update(updateValue)
    }

    @DeleteMapping(value = ["delete-by-id/{id}"])
    @ResponseStatus(HttpStatus.OK)
    fun deleteById(@PathVariable("id") id: String): String {
        return service.deleteById(id)
    }

    @PostMapping(
        value = ["/find"],
        produces = ["application/json"]
    )
    @ResponseStatus(HttpStatus.OK)
    fun find(
        @RequestParam(value = "page", defaultValue = "0") page: Int,
        @RequestParam(value = "size", defaultValue = "10") size: Int,
        @RequestParam(value = "orderBy", defaultValue = "0") orderBy: Int,
        @RequestParam(value = "orderDir", defaultValue = "asc") orderDir: String,
        @RequestBody body: ProductRequest.Find?
    ): FindResponse<List<ProductResponse>> {
        val findRequest = FindRequest(
            page = page,
            size = size,
            orderBy= orderBy,
            orderDir = orderDir,
            filter = body
        )
        return service.find(findRequest)
    }

}
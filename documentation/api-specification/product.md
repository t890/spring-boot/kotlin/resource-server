# API Specification Product
##  Save
* Request   : 
    * Method    : POST
    * Endpoint  : `/api/product/save`
    * Header    : 
        * Content-Type  : application/json
        * Accept        : application/json
    * Body      : 
        ```json
        {
          "name"      : "String, {NotNull, Not Blank}",
          "price"     : "Long, {NotNull, Min(1)}",
          "quantity"  : "Int, {NotNull, Min(0)}"
        }
        ```
* Response  :
    * Code      : 201
    * Status    : Created
    * Body      :
        ```json
        {
          "id"        : "String, unique",
          "name"      : "String",
          "price"     : "Long",
          "quantity"  : "Int"
        }
        ```    
    * Code      : 400
    * Status    : Bad Request
    * Body      :
        ```json
        {
          "name"      : "violeted contraint message",
          "price"     : "violeted contraint message",
          "quantity"  : "violeted contraint message"
        }
        ```    

## Find By id
* Request   : 
    * Method    : GET
    * Endpoint  : `/api/product/find-by-id/{id}`
    * Header    : 
        * Accept : application/json
* Response :
    * Code      : 200
    * Status    : OK
    * Body      :
        ```json
        {
          "id"        : "String, unique",
          "name"      : "String",
          "price"     : "Long",
          "quantity"  : "Int"
        }
        ```
    * Code      : 204
    * Status    : No Content
    * Body      :
        ```
        ``` 

## Update
* Request   : 
    * Method    : PUT
    * Endpoint  : `/api/product/update`
    * Header    : 
        * Content-Type  : application/json
        * Accept        : application/json
    * Body     : 
        ```json
        {
          "id"        : "String, {NotNull, Not Blank}",
          "name"      : "String, {NotNull, Not Blank}",
          "price"     : "Long, {NotNull, Min(1)}",
          "quantity"  : "Int, {NotNull, Min(0)}"
        }
        ```
* Response  :
    * Code      : 200
    * Status    : OK
    * Body      :
        ```json
        {
          "id"        : "String, unique",
          "name"      : "String",
          "price"     : "Long",
          "quantity"  : "Int"
        }
        ```
    * Code      : 204
    * Status    : No Content
    * Body      :
        ```
        ``` 
    * Code      : 400
    * Status    : Bad Request
    * Body      :
        ```json
        {
          "id"        : "violeted contraint message",
          "name"      : "violeted contraint message",
          "price"     : "violeted contraint message",
          "quantity"  : "violeted contraint message"
        }

## Delete
* Request : 
    * Method    : DELETE
    * Endpoint  : `/api/product/delete-by-id/{id}`
* Response :
    * Code      : 200
    * Status    : OK
    * Body      :
        ```
          id
        ```
    * Code      : 204
    * Status    : No Content
    * Body      :
        ```
        ```  

## Find
* Request : 
    * Method        : POST
    * Endpoint      : `/api/product/find`
    * Header        : 
        * Accept        : application/json
    * Query Param   : 
        * page          : Int, {0-n, default 0}
        * PageSize      : Int, {>0, default 10}
        * order         : Int, {default 0}
        * orderDir      : asc/desc, {default asc}
    * Body          :
        (Optional for filter)
        ```json
        {
          "name"      : "String",
          "price"     : "Long",
          "quantity"  : "Int"
        }
        ```
* Response :
    * Code      : 200
    * Status    : OK
    * Body      :
        ```json
        {
          "page"            : "Int",
          "size"            : "Int",
          "totalData"       : "Long",
          "totalPage"       : "Int",
          "totalDataOnPage" : "Int",
          "data": [
            {
              "id"        : "String",
              "name"      : "String",
              "price"     : "Long",
              "quantity"  : "Int"
            },
            {
              "id"        : "String",
              "name"      : "String",
              "price"     : "Long",
              "quantity"  : "Int"
            }
          ]
        }
       ```